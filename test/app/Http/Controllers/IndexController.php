<?php

namespace App\Http\Controllers;

use App\Imports\EmployeeImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class IndexController extends Controller
{
    public function import(Request $request)
    {
        dd($request->file);

        $import = new EmployeeImport();
        Excel::import($import, $request->file('file'));
    }
}
