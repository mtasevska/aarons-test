<?php

namespace App\Imports;

use App\Models\Employee;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class EmployeeImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            Employee::create([
                'date' => $row[0],
                'employee' => $row[1],
                'employer' => $row[2],
                'hours' => $row[3],
                'rate_per_hour' => $row[4],
                'taxable' => $row[5],
                'status' => $row[6],
                'shift_type' => $row[7],
                'paid_at' => $row[8],
            ]);
        }
    }
}
