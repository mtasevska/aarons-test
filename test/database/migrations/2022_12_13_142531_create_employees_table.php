<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->format('YYYY-mm-dd');
            $table->string('employee');
            $table->string('employer');
            $table->integer('hours');
            $table->string('rate_per_hour');
            $table->string('taxable');
            $table->string('status');
            $table->string('shift_type');
            $table->date('paid_at')->format('YYYY-mm-dd H:i:s');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
