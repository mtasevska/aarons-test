import React, { useState } from "react";

export default function Index() {
    const [file, setFile] = useState();

    const fileReader = new FileReader();

    const handleOnChange = (e) => {
        setFile(e.target.files[0]);
    };


    return (
        <div style={{ textAlign: "center" }}>
            <h1>REACTJS CSV IMPORT EXAMPLE </h1>
            <form action="/import" method="POST" encType="multipart/form-data">
                <input
                    type={"file"}
                    id={"csvFileInput"}
                    accept={".csv"}
                    onChange={handleOnChange}
                />

                <button type='submit'>Submit</button>
            </form>
            <input name="_method" type="hidden" value="PATCH"/>
        </div>
    );
}
